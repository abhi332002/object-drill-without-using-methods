//In this file we will implement the internal logic on Object.values method

//define function
function values(obj) {
    //edge case handling
    if (typeof obj !== "object" || obj == null) {
    console.log("Please provide a valid object");
    }

    //store all object vlaues in an array form of string
    let containValues = [];

    // iterate on Object
    for (const key in obj) {
    containValues.push(obj[key]);
    }
    return containValues;
}
  
//exports the function
module.exports = { values };
  