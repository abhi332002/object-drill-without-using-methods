//In this file we will implement the internal logic on Object.keys method

//define function
function mapObject(obj, callback) {
  //edge case handling
  if (typeof obj !== "object" || obj == null) {
    console.log("Please provide a valid object");
  }

  let containObjects = {}

  for (const key in obj) {
    if (obj.hasOwnProperty.call(obj, key)) {
       containObjects[key] = callback(key,obj[key]);
    }
  }
  return containObjects;
}

//exports the function
module.exports = { mapObject };
