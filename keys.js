//In this file we will implement the internal logic on Object.keys method

//define function
function keys(obj) {
  //edge case handling
  if (typeof obj !== "object" || obj == null) {
    console.log("Please provide a valid object");
  }

  //store all key in an array form of string
  let containKeys = [];

  // iterate on Object
  for (const key in obj) {
    containKeys.push(key);
  }
  return containKeys;
}

//exports the function
module.exports = { keys };
