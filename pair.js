//In this file we will implement the internal logic on pair method

//define function
function pair(obj) {
  //edge case handling
  if (typeof obj !== "object" || obj == null) {
    console.log("Please provide a valid object");
  }

  //contain all pairs in the form of array 
  let containPairs = [];

  for (const key in obj) {
    if (obj.hasOwnProperty.call(obj, key)) {
      containPairs.push([key, obj[key]]);
    }
  }
  return containPairs;
}

//exports the function
module.exports = { pair };
