//In this file we will implement the internal logic on default method

//define function
function defaultFun(obj, defaultProps) {
  //edge case handling
  if (typeof obj !== "object" || obj == null) {
    console.log("Please provide a valid object");
  }

  //iterate and check
  for (const key in defaultProps) {

    //check defaultProp has own property and property not exist in original object;
    if (defaultProps.hasOwnProperty(key) && obj[key] === undefined) {
      obj[key] = defaultProps[key];
    }
  }
  return obj;
}

//exports the function
module.exports = { defaultFun };
