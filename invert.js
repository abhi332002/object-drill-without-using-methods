//In this file we will implement the internal logic on invert method

//define function
function invert(obj) {
  //edge case handling
  if (typeof obj !== "object" || obj == null) {
    console.log("Please provide a valid object");
  }

  //it will contian the copy of object 
  let copyObject = {};

  for (const key in obj) {
    if (obj.hasOwnProperty.call(obj, key)) {
      copyObject[obj[key]] = key;
    }
  }

  //return the object that key become value and value become key
  return copyObject;
}

//exports the function
module.exports = { invert };
