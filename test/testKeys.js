//In this file we will tesh our method

const { keys } = require("../keys");

//given object
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//define test function
function testKeys(obj) {
  let allKey = keys(obj);

  console.log(allKey);
}

//call the test function
testKeys(testObject);
