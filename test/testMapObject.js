//In this file we will tesh our method

const { mapObject } = require("../mapObject");


//given object
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//define test function
function testMapObject(obj) {

    let valuesObject = mapObject(obj,(key,val) => val);

    //print the object 
    console.log(valuesObject);
}

//call the test function
testMapObject(testObject);
