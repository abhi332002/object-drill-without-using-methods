//In this file we will tesh our method

const { pair } = require("../pair");



//given object
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//define test function
function testPair(obj) {

  let arrObject = pair(obj);

  //it will return all key and value in array form 
  console.log(arrObject);
}

//call the test function
testPair(testObject);