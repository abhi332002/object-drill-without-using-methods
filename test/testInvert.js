//In this file we will tesh our method

const { invert } = require("../invert");


//given object
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//define test function
function testKeys(obj) {
  let complementObject = invert(obj);

  console.log(complementObject);
}

//call the test function
testKeys(testObject);
