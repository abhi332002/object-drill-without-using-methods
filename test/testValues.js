//In this file we will tesh our method

const { values } = require("../values");


//given object
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

//define test function
function testValues(obj) {

 //take all values of an object
  let allValues = values(obj);

  console.log(allValues);
}

//call the test function
testValues(testObject);
