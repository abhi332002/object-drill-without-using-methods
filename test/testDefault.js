//In this file we will tesh our method

const { defaultFun } = require("../default");

//given object
const testObject = { name: "Bruce Wayne", age: 22, location: "Gotham" };

//define test function
function testDefaultFun(obj) {
  let transformObj = defaultFun(obj, { location: "Delhi" });

  //print the object
  console.log(transformObj);
}

//call the test function
testDefaultFun(testObject);
